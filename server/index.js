const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const {exec_construct_query, build_construct_query_unions,build_select_query,exec_select_query} = require('./sparql');
const execFile = require('child_process').execFile
const path = require('path');
const fs = require("fs");

let SparqlParser = require('sparqljs').Parser;
let parser = new SparqlParser({ skipValidation: true });
let SparqlGenerator = require('sparqljs').Generator;
let generator = new SparqlGenerator();

//app.use(cors({origin: '*'})); // This will also change at deployment time
app.use('/example_queries', express.static('example_queries')); 
app.use('/', express.static('../client')); 
app.use(express.json());
app.use(bodyParser.json());  
app.use(bodyParser.urlencoded({extended: true})); 

const std_prefixes =  {
    wd: 'http://www.wikidata.org/entity/',
    wdt: 'http://www.wikidata.org/prop/direct/',
    wds: 'http://www.wikidata.org/entity/statement/',
    wikibase: 'http://wikiba.se/ontology#',
    p: 'http://www.wikidata.org/prop/',
    ps: 'http://www.wikidata.org/prop/statement/',
    pq: 'http://www.wikidata.org/prop/qualifier/',
    rdfs: 'http://www.w3.org/2000/01/rdf-schema#',
    bd: 'http://www.bigdata.com/rdf#'
}

// Setting middleware
//For deployment we will activate this
//app.use(express.static('client'));  

//sparqlprov engine executable path
//const client_query_path = './sparqlprov-engine/queries/wikidata/client_query.sparql';

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.post('/run', async (req, res) => {
    const temp = require('temp');
    console.log("Client message: " + req.body.query);
    const original_query = req.body.query;   
    let parsed_query = ""; 
    let triples_patterns = [];
    
    try {
        parsed_query = parser.parse(original_query);
        temp.track();
    
        if (parsed_query !== "") {
            triples_patterns = get_patterns(parsed_query);
        }
        temp.open('sparql_query_', function(ferr, info){
            if (!ferr) {
                fs.write(info.fd, original_query, (werr, _bytes) => { if (werr){ console.log(werr); throw werr; } });
                fs.close(info.fd, function(cerr){ if(cerr) { console.log(cerr); throw cerr;} });
                get_provenance(res, triples_patterns, info.path);
            } else {
                console.log(ferr);
                throw ferr;
            }
        });
        temp.cleanup();

    } catch (err) {
       console.log("ERROR SPARQL PARSER");
       console.log(err);
       const responseObj = {"error" : err.toString()}; 
       res.write(JSON.stringify(responseObj));
       res.end();
    }
    
});

app.post('/get_graph', async (req, res) => {
    console.log("Client message: " + req.body.stat_list);
    
    let statements = req.body.stat_list;
    let other_answers = req.body.other_answers;

    let construct_query = build_construct_query_unions(statements);
    let select_query = build_select_query(other_answers);
    let label_pairs = await exec_select_query(select_query);
    await exec_construct_query(construct_query,res,label_pairs,std_prefixes);
});

app.post('/change_graph', async (req, res) => {
    console.log("Client message: " + req.body.stat_list);
    let statements = req.body.stat_list;

    let construct_query = build_construct_query_unions(statements);
    await exec_construct_query(construct_query,res,{"pairs": ""},std_prefixes);
});

const port = process.env.PORT || 3000;
app.listen(port,() => {
    console.log(`Listening on port ${port}...`)
});

/**
* Executes the SPARQLprov implementation with the client query and builds the response with the provenance,
* the rewritten query, the limit and the triples patterns.
* @param {http.ServerResponse} res - The response object that will be sent to the client 
* @param {Array} triples_patterns - Triples patterns of the query 
* @param {string} query_file - A temporary file storing the query string
*/
function get_provenance(res, triples_patterns, query_file) {
    console.log("\n================ SPARQLprov ================\n");
    const response_arr = [];
    const conf = JSON.parse(fs.readFileSync('conf.json'));
    
    //launches the sparqlprov engine
    limit = conf["solutions-limit"];
    const exePath = path.resolve(__dirname, conf["rewrite-script"]);
    const child = execFile(exePath, [query_file, `${limit}`], (error, stdout, stderr) => {       
        if (error !== null) {
            console.log(error);
            const responseObj = {"error" : error}; 
            res.write(JSON.stringify(responseObj));
            fs.rmSync(query_file);
            res.end();
        }
        else {
            let output_arr = JSON.parse(stdout);
            output_arr.solutions.forEach(e => {
                response_arr.push({"answer": e.answer,"annotation": parse_annotation(e.annotation)});
            });
            
            let rquery = format_rewritten_query(decodeURIComponent(output_arr.query).replaceAll("+"," "));
            const responseObj = {"prov" : response_arr, "rewritten-query" : rquery, "limit": limit,"patterns": triples_patterns}; 
            res.write(JSON.stringify(responseObj));
            fs.rmSync(query_file);
            res.end();
        }
    });        
}

/**
* Add prefixes to rewritten query for better readability
* @param {String} rquery - Rewritten query given by SPARQLProv 
*/
function format_rewritten_query(rquery) {
    let parsedQuery = parser.parse(rquery);
    parsedQuery.prefixes = std_prefixes;
    let generatedQuery = generator.stringify(parsedQuery);

    return generatedQuery;
}

/** 
* Parses the annotation from prefix notation to classic infix notation.
* Supports in particular annotations of the form : e_1 * e_2 * ... * e_n and 
* (e_1 * ... * e_k) + (e_i * ... * e_n) + ...
* @param {Array} ann_arr - Annotation array given by SPARQLProv 
*/
function parse_annotation(ann_arr) {
    let final_ann="";
    let operators_stack=[];
    let terms_stack=[];

    //fill the stacks with the operators and the terms
    //(for each operator we also add the number of operands associated)
    if (typeof ann_arr === "string") { //single triple pattern
        terms_stack.push(ann_arr);
        operators_stack.push(["t",1]);
    }
    else if (typeof ann_arr === "object") {
        ann_arr.forEach((sum_elt,index) => {
            if (index === 0) operators_stack.push([sum_elt,ann_arr.length-1]); //add operator-operands couple 
            else {
                //add simple term
                if (typeof sum_elt == "string" || typeof sum_elt=="number") {
                    terms_stack.push(sum_elt);
                    operators_stack.push(["t",1])
                }
                //add product terms
                else {
                    sum_elt.forEach((prod_elt,index) => {
                        if (index === 0) operators_stack.push([prod_elt,sum_elt.length-1]);
                        else terms_stack.push(prod_elt);
                    });
                }
            } 
        });
    }
    
    //unstack the stacks to build final annotation
    if (operators_stack.length > 0) {
        while (operators_stack.length !== 0) {
            let oper = operators_stack.pop();
            let small_ann="";

            //for each operation we build a small annotation
            for (let i=0;i<oper[1];i++) {
                if (oper[1]===1) small_ann+=`${terms_stack.pop()}`;
                else {
                    let str_add="";
                    if (i===0) str_add += "(";
                    str_add += ` ${terms_stack.pop()} ${oper[0]}`;
                    if (i===oper[1]-1) str_add = str_add.slice(0, -1)+")";
                    small_ann+=str_add;
                }
            }
            //add the small annotation obtained in the bottom of the terms stack to build the final annotation
            terms_stack.unshift(small_ann);
            if (operators_stack.length === 1) terms_stack = terms_stack.reverse(); //reverse the terms stack for the last operation to keep right order
        }

        final_ann = terms_stack.pop();
    } else {
        final_ann = "0+0"
    }
    return final_ann;
} 

/** 
* Function to get all the triple patterns of a query
* @param {Object} parsed_query - parsed query object given by the SPARQL parser
*/
function get_patterns(parsed_query) {
    let patterns = [];
    parsed_query.where.forEach(elt => {
        if (elt.type==='bgp') { 
            elt.triples.forEach(triple => {
                let subj = triple.subject.termType === "Variable" ? "VAR" : triple.subject.value;
                let pred = triple.predicate.termType === "Variable" ? "VAR" : triple.predicate.value;
                let obj = triple.object.termType === "Variable" ? "VAR" : triple.object.value;
                patterns.push([subj,pred,obj]);
            });
        }
        else {
            elt.patterns.forEach(pattern => {
                pattern.triples.forEach(triple => {
                    let subj = triple.subject.termType === "Variable" ? "VAR" : triple.subject.value;
                    let pred = triple.predicate.termType === "Variable" ? "VAR" : triple.predicate.value;
                    let obj = triple.object.termType === "Variable" ? "VAR" : triple.object.value;
                    patterns.push([subj,pred,obj]);
                });
                
            });
        }
    });
    return patterns;
}