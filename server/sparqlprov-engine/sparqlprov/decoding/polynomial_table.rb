# coding: utf-8
# frozen_string_literal: true

require 'pry'
require 'set'

# Classes for SPARQLprov
module SPARQLprov
  # Codifies provenance polynomials
  class Polynomial
    attr_reader :type, :data

    def initialize(type, data)
      @type = type
      @data = data
    end

    def self.constant(value)
      new(:constant, value)
    end

    def self.zero
      constant(0)
    end

    def self.one
      constant(1)
    end

    def self.variable(variable)
      new(:variable, variable)
    end

    def self.sum(operands)
      new(:sum, operands)
    end

    def self.product(operands)
      new(:product, operands)
    end

    def self.difference(minuend, subtrahend)
      new(:difference, [minuend, subtrahend])
    end

    def self.aggregate(operands)
      new(:aggregate, operands)
    end

    def simplify!
      @data.each(&:simplify!) if %i[sum product difference aggregate].include? @type
      remove_zeros_in_sums!
      remove_ones_in_products!
      remove_trivial_sums!
      remove_trivial_products!
    end

    def remove_zeros_in_sums!
      @data.reject! { |operand| operand.type == :constant && operand.data.zero? } if @type == :sum
    end

    def remove_ones_in_products!
      @data.reject! { |operand| operand.type == :constant && operand.data == 1 } if @type == :product
    end

    def remove_trivial_sums!
      return unless @type == :sum

      case @data.size
      when 0
        @data = 0
        @type = :constant
      when 1
        @type = @data.first.type
        @data = @data.first.data
      end
    end

    def remove_trivial_products!
      return unless @type == :product

      case @data.size
      when 0
        @data = 1
        @type = :constant
      when 1
        @type = @data.first.type
        @data = @data.first.data
      end
    end

    def to_s
      case @type
      when :constant then @data.to_s
      when :variable then "<#{@data}>"
      else
        args = @data.map(&:to_s).join(' ')
        operator = { sum: '+', product: '*', difference: '-', aggregate: 'd' }[@type]
        "( #{operator} #{args} )"
      end
    end

    def to_json
      case @type
      when :constant then @data.to_s
      when :variable then "\"<#{@data}>\""
      else
        args = @data.map(&:to_json).join(', ')
        operator = { sum: '"+"', product: '"*"', difference: '"-"', aggregate: '"d"' }[@type]
        "[#{operator}, #{args}]"
      end
    end

    def to_html(var_names)
      case @type
      when :constant then "<span class=\"poly-constant\">#{@data}</span>"
      when :variable then "<span class=\"poly-variable\"><a href=\"#{@data}\">#{var_names[@data]}</a></span>"
      else
        args = @data.map { |poly| poly.to_html(var_names) }.join(' ')
        operator = { sum: '⊕', product: '⊗', difference: '⊖', aggregate: 'δ' }[@type]
        "<span class=\"poly-operator\">( #{operator} #{args} )</span>"
      end
    end

    def lineage
      case @type
      when :constant then []
      when :variable then [@data]
      else
        # TODO: in the following line we eliminate nil values with compact.
        # nil values must be detected before.
        @data.compact.map(&:lineage).inject([], &:+).uniq
      end
    end
  end

  # Provenance variables are codified as strings
  class ProvenanceVariable
    attr_accessor :name

    def initialize(name)
      @name = name
    end

    def root?
      !@name.include? '_' and !@name.empty?
    end

    def prefix?(prefix)
      @name.start_with? prefix.name
    end

    def subvariable
      return nil if root?

      sub_name = @name.sub(/^[a-z]+_([0-9]+_)?/, '')
      ProvenanceVariable.new sub_name
    end

    def child?(child)
      return false if child.root?

      child.prefix? self and child.subvariable.root?
    end

    def ==(other)
      @name == other.name
    end

    def eql?(other)
      @name == other.name
    end

    def hash
      @name.hash
    end
  end

  # A polynomial table
  class PolynomialTable
    attr_reader :rows

    def initialize(variables)
      @variables_map = variables.map.with_index.to_h
      @rows = Set.new
    end

    def add(row)
      @rows.add(row)
    end

    def variables
      @variables_map.map.sort { |x, y| x[1] <=> y[1] }.map { |x| x[0] }
    end

    def ==(other)
      variables == other.variables && @rows == other.rows
    end

    def root
      @root ||= @variables_map.select do |variable, _|
        variable.root?
      end.keys.first
    end

    def root_children
      @root_children ||= @variables_map.select do |variable, _|
        root.child? variable
      end.keys.sort_by(&:name)
    end

    def branch_variables(parent_variable)
      variables.select do |variable|
        variable.prefix? parent_variable
      end
    end

    def subtable_variables(parent_variable)
      branch_variables(parent_variable).map(&:subvariable)
    end

    def value(row, variable)
      var = if variable.instance_of? String
              ProvenanceVariable.new variable
            else
              variable
            end
      row[@variables_map[var]]
    end

    def subrow(parent_variable, row)
      old_variables = branch_variables(parent_variable)
      new_row = []
      old_variables.each.with_index do |old_variable, new_variable_index|
        new_row[new_variable_index] = value(row, old_variable)
      end
      new_row
    end

    def subtables(parent_variable)
      subtables_map = {}
      @rows.each do |row|
        group_by_value = value(row, parent_variable)
        next if group_by_value.nil?
        next if group_by_value.is_a?(String) && group_by_value.empty?

        unless subtables_map.include?(group_by_value)
          subtables_map[group_by_value] =
            PolynomialTable.new(subtable_variables(parent_variable))
        end
        subtables_map[group_by_value].add subrow(parent_variable, row)
      end
      subtables_map
    end

    def polynomial
      case root.name
      when 'statement'
        statement_polynomial
      when 'sum'
        sum_polynomial
      when 'osum'
        osum_polynomial
      when 'product'
        product_polynomial
      when 'difference'
        difference_polynomial
      when 'aggregate'
        aggregate_polynomial
      end
    end

    def statement_polynomial
      return Polynomial.zero if @rows.empty?

      polynomial_value = value(@rows.first, 'statement')
      case polynomial_value
      when ''
        Polynomial.zero
      when nil
        Polynomial.zero
      when /^[0-9]+$/
        Polynomial.constant(polynomial_value.to_i)
      else
        Polynomial.variable(polynomial_value)
      end
    end

    def sum_polynomial

      child = root_children.first
      operands = subtables(child).map do |_, table|
        table.polynomial
      end
      Polynomial.sum(operands)
    end

    def aggregate_polynomial
      child = root_children.first
      operands = subtables(child).map do |_, table|
        table.polynomial
      end
      Polynomial.aggregate(operands)
    end

    def osum_polynomial
      children = root_children.map do |child|
        operands = subtables(child).map do |_, table|
          table.polynomial
        end
        Polynomial.sum(operands)
      end
      Polynomial.sum(children)
    end

    def product_polynomial
      children = root_children.map do |child|
        operands = subtables(child).map do |_, table|
          table.polynomial
        end
        Polynomial.sum(operands)
      end
      Polynomial.product(children)
    end

    def difference_polynomial
      children = root_children
      minuend = subtables(children[0]).values.first.polynomial
      subtrahend = subtables(children[1]).values.first.polynomial
      Polynomial.difference(minuend, subtrahend)
    end
  end

  # A relation annotated with how-provenance
  class ProvenanceRelation
    def initialize(raw_answers, prefix = 'prov')
      @raw_answers = raw_answers
      @prefix = prefix
    end

    def provenance_headers
      @provenance_headers ||= @raw_answers.variable_names.select do |header|
        /^#{@prefix}_/.match(header.to_s)
      end
    end

    def solution_headers
      @solution_headers ||= @raw_answers.variable_names - provenance_headers
    end

    def annotated_answers
      if @annotated_answers.nil?
        @annotated_answers = {}

        polynomial_table_headers = provenance_headers.map do |header|
          ProvenanceVariable.new(header.to_s.sub(/^#{@prefix}_/, ''))
        end

        @raw_answers.each do |row|
          answer = solution_headers.map { |var| [var, row[var]] }.to_h
          unless @annotated_answers.include? answer
            @annotated_answers[answer] =
              PolynomialTable.new(polynomial_table_headers)
          end
          @annotated_answers[answer].add(provenance_headers.map { |x| row[x] })
        end

        @annotated_answers.each do |answer, table|
          @annotated_answers[answer] = table.polynomial
          @annotated_answers[answer].simplify!
        end
      end

      @annotated_answers
    end

    def include?(answer)
      annotated_answers.include?(answer)
    end

    def [](answer)
      annotated_answers[answer]
    end

    def to_json
      rows = annotated_answers.map do |answer, annotation|
        "{ \"answer\": #{answer.to_json}, \"annotation\": #{annotation.to_json} }"
      end

      "[#{rows.join(', ')}]"
    end
  end

  def self.decode(csv_data, prefix = 'prov')
    annotated_answers = {}

    provenance_headers = csv_data.headers.select do |header|
      /^#{prefix}_/.match(header.to_s)
    end
    polynomial_table_headers = provenance_headers.map do |header|
      ProvenanceVariable.new(header.to_s.sub(/^#{prefix}_/, ''))
    end
    solution_headers = csv_data.headers - provenance_headers

    csv_data.each do |row|
      answer = solution_headers.map { |var| [var, row[var]] }.to_h
      unless annotated_answers.include? answer
        annotated_answers[answer] =
          PolynomialTable.new(polynomial_table_headers)
      end
      annotated_answers[answer].add(row.values_at(*provenance_headers))
    end

    annotated_answers.each do |answer, table|
      annotated_answers[answer] = table.polynomial
    end
  end
end
