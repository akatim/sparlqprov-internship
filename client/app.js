const ROOT_APP = "http://localhost:3000"

const filterNodesById = (nodes,id) => (
    nodes.filter(n => (n.id === id))
);

/** 
* Builds a graph (nodes and links) from RDF triples.
* @param {Array} triples - RDF triples
*/
const triplesToGraph = (triples) => {
    let graph={nodes:[], links:[]};
    
    triples.forEach( (triple) => {
        let subjId = triple.subject;
        let predId = triple.predicate;
        let objId = triple.object;
        let statId = triple.statement;
        
        let subjNode = filterNodesById(graph.nodes, subjId)[0];
        let objNode  = filterNodesById(graph.nodes, objId)[0];
        
        if(subjNode==null){
            subjNode = {id:subjId, label:subjId};
            graph.nodes.push(subjNode);
        }
        
        if(objNode==null){
            objNode = {id:objId, label:objId};
            graph.nodes.push(objNode);
        }
        
        graph.links.push({source:subjNode, target:objNode, predicate:predId, statement: statId});
    });
    
    return graph;
}

let triples_graph = [];
let statements_id = {};
let id_statements={};
let id_count=1;
let prov = [];
let mappings = {};
let contract_mappings = {};
let contract_mappings_rev = {};
let triples_patterns=[];
let rawQuery = "";
let limit = 0;

let graph_height = 600;
let graph_body_select = d3.select(".graph-body");
let graph_width = d3.select(".graph-body").node().getBoundingClientRect().width;
let svg = graph_body_select
            .classed('svg',true)
            .append("svg")
            .attr("height", graph_height);


let g = svg.append("g");

let zoom = d3.zoom()
            .on('zoom', () => {
                g.attr('transform', d3.event.transform);
        });

svg.call(zoom);

let graph = triplesToGraph(triples_graph);
let links = g.selectAll("line").data(graph.links);
let nodes = g.selectAll("circle").data(graph.nodes);
let linkTexts = g.selectAll(".link-text").data(graph.links);
let nodeTexts = g.selectAll(".node-text").data(graph.nodes);

let loadText = svg.append("text")   
                    .text("Loading...")
                    .attr("x","45%")
                    .attr("y","50%")
                    .style("opacity", 0);

let errorText = svg.append("text")   
                    .text("There is no graph to display!")
                    .attr("x","35%")
                    .attr("y","50%")
                    .style("opacity", 0).style("fill", "red");
/** 
* Update all the elements of the RDF graph
*/
function update() {
    g.remove();
    g = svg.append("g");

    graph = triplesToGraph(triples_graph);
    console.log(graph);

    links = g.selectAll("line")
            .data(graph.links)
            .enter()
            .append("line")
            .attr("marker-end", "url(#end)")
            .attr("class", "link")
            .attr("id",d => d.statement);

    nodes = g.selectAll("circle")
            .data(graph.nodes)
            .enter()
            .append("circle")
            .attr("r", 10)
            .attr("class", "node")
            .attr("id",d => d.id)
            .call(
                d3
                .drag()
                .on("start", dragStart)
                .on("drag", drag)
                .on("end", dragEnd)
            );

    linkTexts = g.selectAll(".link-text")
                .data(graph.links)
                .enter()
                .append("text")
                .attr("class", "link-text")
                .text(d => d.predicate);

    nodeTexts = g.selectAll(".node-text")
                .data(graph.nodes)
                .enter()
                .append("text")
                .attr("class", "node-text")
                .text(d => d.label);

    function ticked() {
        nodes.attr("cx", d => d.x).attr("cy", d => d.y);

        links
            .attr("x1", d => d.source.x)
            .attr("y1", d => d.source.y)
            .attr("x2", d => d.target.x)
            .attr("y2", d => d.target.y);
        
        nodeTexts
            .attr("x", d => d.x + 12)
            .attr("y", d => d.y + 3 );
            
        linkTexts
            .attr("x", d => 4 + (d.source.x + d.target.x)/2)
            .attr("y", d => (d.source.y + d.target.y)/2);
    }

    function dragStart(d) {
        simulation.alphaTarget(0.01).restart();
        d.fx = d.x;
        d.fy = d.y;
    }
    
    function drag(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    }

    function dragEnd(d) {
        simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
    }

    g.append("svg:defs").selectAll("marker")
                        .data(["end"])
                        .enter().append("svg:marker")
                        .attr("id", String)
                        .attr("viewBox", "0 -5 10 10")
                        .attr("refX", 24)
                        .attr("refY", -0.5)
                        .attr("markerWidth", 6)
                        .attr("markerHeight", 6)
                        .attr("orient", "auto")
                        .append("svg:polyline")
                        .attr("points", "0,-5 10,0 0,5");

    let simulation = d3.forceSimulation(graph.nodes);

    simulation
        //.force("link", d3.forceLink(graph.links).id(d => d.id).distance(300).strength(0.4))
        .force("center", d3.forceCenter(graph_width/2,graph_height/2))
        .force("charge",d3.forceManyBody().strength(-50))
        .on("tick", ticked);
}

let runBtn = d3.select('.run-btn').select('input');
let queryBody = d3.select('.query-body')
let rQueryBody = d3.select('.rquery-body')

queryBody.append('textarea')
        .attr('name','query')
        .attr('id', 'query');

let copyBtn = rQueryBody.append("button")
                        .attr("type","button")
                        .style("opacity", 0)
                        .on('click',() => {
                            navigator.clipboard.writeText(rQueryBody.select("#RewrittenQuery").select('p').property('textContent'));
                        });
copyBtn.append("ion-icon")
        .attr("name","copy-outline");
        
                     
const openQueryTab = (evt, tabName) => {
    // Declare all variables
    let i, tabcontent, tablinks;
  
    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
  
    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
  
    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";

    if (tabName==='RewrittenQuery') copyBtn.style("opacity", 1);
    else copyBtn.style("opacity", 0);
} 
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click()

let run_btn_select = d3.select(".run-btn");
let loadIcon = run_btn_select.append("ion-icon")
                            .attr("name","hourglass-outline")
                            .style("opacity", 0);

/** 
* Callback function when run button is clicked. 
*/
const runEvent = async () => {
    //Get query entered
    rawQuery = editor.getValue();
    if (rawQuery !== "") {
        //Send data to server (POST)
        data = {query: rawQuery}

        g.remove();
        loadText.style("opacity", 1);
        loadIcon.style("opacity", 1);
        errorText.style("opacity", 0);
        clearInterface();

        const response = await fetch(`${ROOT_APP}/run`,{
            method: "POST",
            mode: 'cors',
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(data)
        });
        response.json().then(receive_prov);
    }
}
runBtn.on('click', runEvent);

let exButtons = d3.selectAll('.button-example');
exButtons.on('click', async () => { 
    const idTargetBtn = parseInt(d3.event.target.id.replace('exbt', ''));
    const url = `${ROOT_APP}/example_queries/q${idTargetBtn}.sparql`;
    fetch(url)
    .then( r => r.text() )
    .then( t => {  
        const theEditor = document.querySelector('.CodeMirror').CodeMirror; 
        theEditor.setValue(t);
    });
});

/** 
* Receive the provenance response sent by server 
* and send data to server to get the RDF graph
* @param {JSON} json - json response sent by server
*/
const receive_prov = async (json) => {
    if (Object.keys(json)[0]==="error") {
        console.log("Error");
        errorText.style("opacity", 1);
        loadIcon.style("opacity", 0);
        loadText.style("opacity", 0);
        rQueryBody.select("#OriginalQuery").select('p').text(Object.values(json)[0]);
        tableDiv.html("<span style=\"color: red\">Error - No results to display</span>");
        errorText.style("opacity", 1);
        errorText.style("color", "red");
        loadIcon.style("opacity", 0);
        loadText.style("opacity", 0);
    } else {
        rQueryBody.select("#OriginalQuery").select('p').text(rawQuery);
        rQueryBody.select("#RewrittenQuery").select('p').text(json["rewritten-query"]);
        tableDiv.html("");
        if (json.prov.length === 0) {
            tableDiv.text("No matching records found");
            errorText.style("opacity", 1);
            loadIcon.style("opacity", 0);
            loadText.style("opacity", 0);
        } else {
            //update
            prov=[];
            statements_id={};
            id_statements={};

            limit = json["limit"];
            rawQuery = rawQuery + ` LIMIT ${limit}`;
            rQueryBody.select("#OriginalQuery").select('p').text(rawQuery);
            rQueryBody.select("#RewrittenQuery").select('p').text(json["rewritten-query"]);

            triples_patterns = json["patterns"];

            let st = process_prov(json.prov);
            let data = {stat_list: st[0], other_answers: st[1]};
            const response = await fetch(`${ROOT_APP}/get_graph`,{
                method: "POST",
                mode: 'cors',
                headers: {"Content-Type": "application/json"},
                body: JSON.stringify(data)
            });
            response.json().then(process_answers);
        }
    }
}

/** 
* Process the provenance results sent by server 
* @param {JSON} json_prov - json response sent by server
*/
const process_prov = (json_prov) => {
    let statements = [];
    let other_answers=[];
    let id_count = 1;

    json_prov.forEach((p,index) => {        
        let ann_arr = p.annotation.split(" ");
        ann_arr.forEach(w => { 
            if ( w !== ")" && w !== "(" && w !== "+" && w !== "*" && w!=="0" && w!=="-") {   
                if (!(Object.keys(statements_id).includes(w))) {
                    statements_id[w] = `e${id_count}`;
                    id_statements[`e${id_count}`] = w;
                    id_count++;
                }
                if (index===0) statements.push(w);
            }
        });

        if (index !== 0) {   
            Object.keys(p.answer).forEach(key => {
                other_answers.push(p.answer[key]);
            });
        }

        prov.push({"answer": p.answer, "annotation":formatProvAnn(p.annotation)});
    });

    return [statements,other_answers];
}

/** 
* Process the information sent by server for the RDF graph
* @param {JSON} json_prov - json response sent by server
*/
const process_answers = (json) => {
    //update
    triples_graph=[];

    //label mappings object with keys without prefixes
    contract_mappings = {...json.mappings_prefix};
    Object.keys(contract_mappings).forEach(key => {
        contract_mappings[key] = contract_mappings[key].split(":").pop();
        contract_mappings[contract_mappings[key]] = key;
        delete contract_mappings[key];
    });

    //labels for patterns
    triples_patterns.forEach(triple => {
        triple.forEach((term,index) => {
            if (term !== "VAR") triple[index] = Object.keys(contract_mappings).includes(term.split("/").pop()) ? contract_mappings[term.split("/").pop()] : term;
        })
    });

    //swap mappings keys values (useful when clicking on elements in resprov table)
    Object.keys(json.mappings).forEach(key => {
        mappings[json.mappings[key]] = key;
    });
    Object.keys(json.pairs).forEach(key => {
        mappings[json.pairs[key]] = key;
    });
    Object.keys(contract_mappings).forEach(key => {
        contract_mappings_rev[contract_mappings[key]] = key;
    });

    json.quads.forEach(quad => {
        add_quads(triples_graph,quad,json);
    });
    update();
    
    prov.forEach(p => {        
        Object.keys(p.answer).forEach(key => {
            p.answer[key] = Object.keys(json.mappings).includes(p.answer[key]) ? json.mappings[p.answer[key]] : p.answer[key];
            p.answer[key] = Object.keys(json.pairs).includes(p.answer[key]) ? json.pairs[p.answer[key]] : p.answer[key];
        });
    });
    highlightGraph("");
    table = tabulate(prov);
    loadText.style("opacity", 0);
    loadIcon.style("opacity", 0);
}

/** 
* Add a quad to an array 
* @param {Array} array - array of RDF triples and their statements
* @param {Array} quad - RDF triple + statement
* @param {JSON} json - json response sent by server
*/
const add_quads = (array,quad,json) => {
    let stat = "";
    if (quad[3] != "") {
        stat = '<'+quad[3]+'>';
    } 
    //take the label if it exists
    let subj = (Object.keys(json.mappings).includes(quad[0]) ? json.mappings[quad[0]] : quad[0]);
    let pred = (Object.keys(json.mappings).includes(quad[1]) ? json.mappings[quad[1]] : quad[1]);
    let obj = (Object.keys(json.mappings).includes(quad[2]) ? json.mappings[quad[2]] : quad[2]);

    //we want subjects and objects to be related by only one predicate
    let add = true;
    array.forEach((triple,index) => {
        if ((triple.subject===subj && triple.object===obj) || (triple.subject===obj && triple.object===subj) || (triple.object=== subj && triple.subject===obj) ) {
            if (stat !== "") array[index] = {subject: subj, predicate: `${stat != "" ? statements_id[stat]+" - " : ""}${pred}`, object: obj, statement: stat};
            add = false;
        }
    });

    if (subj !== obj && add)
        array.push({subject: subj, predicate: `${stat != "" ? statements_id[stat]+" - " : ""}${pred}`, object: obj, statement: stat});
}

let tableDiv = d3.select(".resprov-table"); 
let table = tableDiv.append("table").attr("id","resprov-table");
let tableBtns = tableDiv.append("div").attr("class","table-buttons");

function clearInterface(){
    tableDiv.html("");
    rQueryBody.select("#OriginalQuery").select('p').text("");
    rQueryBody.select("#RewrittenQuery").select('p').text("");
}                

/** 
* Builds the results-provenance table
* @param {Array} data - results and provenance data
*/
const tabulate = (data) => {
    table.remove();
    tableBtns.remove();
    table = tableDiv.append("table").attr("id","resprov-table");
    tableBtns = tableDiv.append("div").attr("class","table-buttons");
    thead = table.append("thead"),
    tbody = table.append("tbody");

    let columns = [...Object.keys(data[0].answer), "Provenance", "", ""];
           
    // append the header row
    thead.append("tr")
        .selectAll("th")
        .data(columns)
        .enter()
        .append("th")
        .text(column => column);

    // create a row for each object in the data
    let rows = tbody.selectAll("tr")
                    .data(data)
                    .enter()
                    .append("tr");

    // create a cell in each row for each column
    let cells = rows.selectAll("td")
                .data((row,index) => {
                    return columns.map(c => {
                        return {value: (c !== "Provenance" ? row.answer[c] : (index !== 0 ? row["annotation"] : getClickAnn(row["annotation"]))), 
                                link: (c !== "Provenance" ? true : false) };
                    });
                })
                .enter()
                .append("td")
                .append("a")
                .html(d => d.value) 
                .on("click",d => (d.link ? window.open(mappings[d.value],'_blank') : ''))
                .attr("class",d => (d.link ? 'alink' : ''));

    //"show" buttons
    let btn_cell = rows.append("td").attr("class","cell-btn");                
    btn_cell.filter((d,index) => index !==0)
            .append("button")
            .on("click",(d,index) => changeGraph(d,index))
            .attr("type","button")
            .append("ion-icon")
            .attr("name","eye-outline");

    //pagination
    if (data.length > 10) { 
        d3.select(".table-buttons").datum({portion : 0});
        tableBtns.append("button")
                .attr("id","left")
                .text('<')
                .on ("click", d => {
                    if ( d.portion-10 >= 0) {
                        d.portion -= 10;
                        redraw(d.portion);
                    }
                    tableBtns.select("p").text(`${d.portion/10 + 1} / ${Math.ceil(prov.length/10)}`);
                });

        tableBtns.append("button")
                .attr("id","right")
                .text('>')
                .on ("click", d => {
                    if (d.portion+10 < prov.length ) {
                        d.portion += 10;
                        redraw(d.portion);
                    }
                    tableBtns.select('p').text(`${d.portion/10 + 1} / ${Math.ceil(prov.length/10)}`);
                });
        redraw(0);
        tableBtns.append("p").text(`1 / ${Math.ceil(prov.length/10)}`);
    }
    
    //solutions highlight in table
    let minus_sol = rows.filter(d => !(d.annotation.includes(" 0 ")) && d.annotation.includes("&#8854")).attr("bgcolor","#dbdbdb");
    let first_row = rows.filter((d,index) => index === 0).attr("bgcolor","#7ed5fc");
    return table;
}

/** 
* Callback function to redraw the table when clicking on pagination buttons 
* @param {Number} start - index from which we build the table 
*/
const redraw = (start) => {
    tbody.selectAll("tr")
        .style("display", (d,i) => i >= start && i < start + 10 ? null : "none")
}

/** 
* Callback function to change graph when the "show" button is clicked
* @param {Object} d - data of the clicked row
* @param {Number} index - index in the prov table 
*/
const changeGraph = async (d,index) => {
    statements = getStatementsList(d.annotation);
    //swap with first element in prov table
    let temp = prov[0];
    prov[0] = prov[index+1];
    prov[index+1] = temp;

    //update
    g.remove();
    loadText.style("opacity", 1);
    loadIcon.style("opacity", 1);
    errorText.style("opacity", 0);
    rawQuery = editor.getValue() + ` LIMIT ${limit}`;
    rQueryBody.select("#OriginalQuery").select('p').text(rawQuery);

    let data = {stat_list: statements};
    const response = await fetch(`${ROOT_APP}/change_graph`,{
        method: "POST",
        mode: 'cors',
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(data)
    });
    response.json().then(process_answers);
}

/** 
* Function to get the list of statements in a formatted prov annotation
* @param {String} form_ann - prov annotation in html format 
*/
const getStatementsList = (form_ann) => {
    let statements = [];
    const regex = new RegExp('e<sub>\\d+<\/sub>');
    
    form_ann.split(" ").forEach(w => {
        if (regex.test(w) ) {   
            let id = w.split(">")[1].split("<")[0];
            statements.push(id_statements[`e${id}`]);
        }
    });
    
    return statements;
}

/** 
* Highlight the clicked statement (RDF triple associated) in the graph
* @param {String} stat_clicked - clicked statement 
*/
const highlightGraph = (stat_clicked) => {
    //highlight edges
    let links_stat = links.filter(d => d.statement !== "");
    links_stat.attr("class","link-selected");

    let links_text_stat = linkTexts.filter(d => d.statement !== "");
    links_text_stat.attr("class","link-text-selected");

    //highlight nodes
    const triples_st = triples_graph.filter(triple => triple.statement !== "");
    const nodes_arr = [];
    triples_st.forEach(triple => {
        if (!(nodes_arr.includes(triple.subject))) nodes_arr.push(triple.subject);
        if (!(nodes_arr.includes(triple.object))) nodes_arr.push(triple.object);
    });
    let nodes_stat = nodes.filter(d => nodes_arr.includes(d.id));
    nodes_stat.attr("class","node-selected");
    let answers = Object.values(prov[0].answer);
    let nodes_answers = nodes_stat.filter(d => answers.includes(d.id));
    nodes_answers.attr("class","node-answer");

    //highlight triple when statement is clicked 
    if (stat_clicked !== "") {
        let links_stat_click = links.filter(d => d.statement === stat_clicked);
        links_stat_click.attr("class","link-selected-click");
    
        let links_text_stat_click = linkTexts.filter(d => d.statement === stat_clicked);
        links_text_stat_click.attr("class","link-text-selected-click");
        
        const nodes_click_arr = [];
        triples_st.forEach(triple => {
            if (triple.statement === stat_clicked) {
                nodes_click_arr.push(triple.subject);
                nodes_click_arr.push(triple.object);
            }
        });

        let nodes_clicked = nodes_stat.filter(d => nodes_click_arr.includes(d.id));
        nodes_clicked.attr("class","node-selected-click");
        let nodes_answers_click = nodes_answers.filter(d => nodes_click_arr.includes(d.id));
        nodes_answers_click.attr("class","node-answer-click");
        
    }
}

/** 
* Function to transform a prov annotation in html format
* @param {String} raw_ann - raw provenance annotation 
*/
const formatProvAnn = (raw_ann) => {
    let ann_arr = raw_ann.split(" ");

    //replace uri with id
    ann_arr.forEach((w,index) => {
        if ( w !== ")" && w !== "(" && w !== "+" && w !== "*" && w!=="0" && w!=="-") {         
            if (statements_id[w] !== undefined) {
                ann_arr[index] = statements_id[w];
            }
        }
    });

    //html format for display
    ann_arr.forEach((w,index) => {
        if ( w !== ")" && w !== "(") {
            switch (w) {
                case "+": ann_arr[index] = "&#8853"; break;              
                case "*": ann_arr[index] = "&#8855"; break;
                case "0": ann_arr[index] = "0"; break;
                case "-": ann_arr[index] = "&#8854"; break;
                default: {
                    let id = w.split("e")[1]
                    ann_arr[index] = `e<sub>${id}</sub>`;   
                }
            }
        }
    });
    
    let form_ann = ann_arr.join(" ");
    return form_ann;
};

/** 
* Makes statements clickable in a prov annotation
* @param {String} form_ann - prov annotation in html format 
*/
const getClickAnn = (form_ann) => {
    let ann_arr = form_ann.split(" ");
    const regex = new RegExp('e<sub>\\d+<\/sub>');
    
    ann_arr.forEach((w,index) => {
        if (regex.test(w)) {   
            let id = w.split(">")[1].split("<")[0];
            ann_arr[index] = `<a class="astat" onclick = "highlightStat(${id})"> ${w} </a>`
        }
    });
   
    let click_ann = ann_arr.join(" ");
    return click_ann;
}

/** 
* Callback function to highlight a statement when it's clicked
* @param {Number} id - clicked statement index
*/
const highlightStat = (id) => {
    let stat_clicked = id_statements[`e${id}`]; //get URI of clicked statement
    highlightGraph(stat_clicked); //highlight statement in the graph

    //get RDF triple associated to clicked statement 
    let triple_clicked=[];
    triples_graph.forEach(triple => {
        if (triple.statement===stat_clicked) {
            triple_clicked = [triple.subject,triple.predicate.split(" - ")[1],triple.object];
        }
    });

    //for each triple pattern we count the number of matches with the clicked statement
    let nb_matchs = new Array(triples_patterns.length).fill(0);
    triples_patterns.forEach((pattern,index)=> {
        for (let i = 0; i < 3; i++) 
            if (pattern[i] === triple_clicked[i]) nb_matchs[index] += 1;
        
    });
    let pattern_index = nb_matchs.indexOf(Math.max(...nb_matchs)); //we take the pattern which has the max number of matches
    let pattern_match = [...triples_patterns[pattern_index]]; //clone pattern array

    //replace labels of the pattern by their id (reverse mapping: label => id)
    pattern_match.forEach((pattern_elt,index) => {
        if (pattern_elt !== "VAR") pattern_match[index] = Object.keys(contract_mappings_rev).includes(pattern_elt) ? contract_mappings_rev[pattern_elt] : pattern_elt;
    });

    //build the regex from the matched pattern
    let pattern_regex = "";
    pattern_match.forEach((elt,index) => {
       if (index === 0 && rawQuery.includes(";")) ;
       else {
        if (elt === "VAR") pattern_regex += "\\?[a-zA-Z0-9]+\\s*";
        else pattern_regex += `[a-zA-Z0-9:\/.<>]+${elt}\\s*`;
       }
    });

    //find and replace the regex
    const regex = new RegExp(pattern_regex);
    let highlighted_query = rawQuery.replaceAll("<","&lt");
    highlighted_query = highlighted_query.replaceAll(">","&gt");
    rQueryBody.select("#OriginalQuery").select('p').html(highlighted_query.replace(regex,'<em>'+highlighted_query.match(regex)[0]+'</em>'));
}

//SPARQL code editor
let editor = CodeMirror.fromTextArea(document.getElementById('query'), {
    lineNumbers: true,
    placeholder: "Enter a SPARQL query here...",
    styleActiveLine: true,
    matchBrackets: true,
    mode: "application/sparql-query"
});        
editor.setOption("theme", 'default');

